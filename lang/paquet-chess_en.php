<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

	'chess_titre' => 'Chess',
	'chess_slogan' => 'Allow to display chess games with PGN files',
	'chess_description' => 'This plugin allows to read chess games on line with their PGN file.
  To insert the widget you have just to write in your article the tag &#60;chessXX&#62; where XX is the number of the article, then upload the PGN files.',

 );
?>